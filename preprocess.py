import json
import nltk

def rem_ne(sent, map = None, isSent = False):
    sentences = nltk.sent_tokenize(sent)
    tokenized = [nltk.word_tokenize(sentence) for sentence in sentences]
    pos_tags  = [nltk.pos_tag(sentence) for sentence in tokenized]
    result = list()
    ne_counter = 0
    ne_map = dict()

    new_pos_tags = list()
    i = 0
    pos_tags = pos_tags[0]
    while i < len(pos_tags):
        start_i = i
        new_pos_tag = pos_tags[i]
        if pos_tags[i][1] == 'NNP':
            while i + 1 < len(pos_tags) and pos_tags[i + 1][1] == 'NNP':
                i += 1
                new_pos_tag = (new_pos_tag[0] + ' ' + pos_tags[i][0], 'NNP')
        new_pos_tags.append(new_pos_tag)
        i += 1

    year_counter = 0
    for word in new_pos_tags:
        if len(word) == 2:
            if word[1] == 'CD' and word[0] != 'AAA':
                result.append('<year>')
                ne_map['year ' + str(year_counter)] = word[0]
                year_counter += 1
            elif word[1] == 'NNP' and word[0] != 'AAA':
            	key = None;
            	for k, v in ne_map.items():
            		if v == word[0]:
            			key = k
            	if key != None:
            		result.append('<' + str(key) + '>')
            	else:
	                ne_map[ne_counter] = word[0]
	                result.append('<' + str(ne_counter) + '>')
	                ne_counter += 1
            else:
                result.append(word[0])
    result = ' '.join(result)
    return result, ne_map, {v: k for k, v in ne_map.items()}

f = open('data/train-v1.1.json')
data = json.loads(f.readline())['data']
pairs = list()
sent_out = list()
q_out = list()
ans_out = list()
map_out = list()
counter = 0
test = 0
for topic in data:
	counter += 1
	print(str(counter)+'/'+str(len(data)))
	for paragraph in topic['paragraphs']:
		context = paragraph['context']
		sentences = context.split('.')
		for q in paragraph['qas']:
			question = q['question']
			ans_start = q['answers'][0]['answer_start']
			ans = q['answers'][0]['text']
			curr_len = 0
			for i, sent in enumerate(sentences):
				curr_len += len(sent)
				if curr_len > ans_start:
					if ans not in sent and i < len(sentences)-1:
						sent += '' + sentences[i+1]
					ans = ans.replace('.', '')
					sent = sent.replace(ans, 'AAA')
					if 'AAA' in sent:
						sent, new_map_orig, new_map = rem_ne(sent, None, True)
						question, _, _ = rem_ne(question, new_map, False)
						new_map_orig['AAA'] = ans
						# print(new_map)
						# ans, _ = rem_ne(ans,new_map)
						pairs.append(sent + '|' + question + '|' + ans)
						sent_out.append(sent)
						q_out.append(question)
						ans_out.append(ans)
						map_out.append(str(new_map_orig))
					break

open('data/train-v1.1.pairs.no_ne', 'w').write('\n'.join(pairs))
open('data/train-v1.1.sentences.no_ne', 'w').write('\n'.join(sent_out))
open('data/train-v1.1.questions.no_ne', 'w').write('\n'.join(q_out))
open('data/train-v1.1.answers.no_ne', 'w').write('\n'.join(ans_out))
open('data/train-v1.1.map.no_ne', 'w').write('\n'.join(map_out))